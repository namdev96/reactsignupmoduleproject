import React, { Component } from 'react'
import { register } from './UserFunctions'
import '../Style.css';

class EmailVarify extends Component {
  constructor() {
    super()
    

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  onSubmit(e) {
    e.preventDefault()

   
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-6 mt-5 mx-auto">
            <form noValidate onSubmit={this.onSubmit}>
              <p className ="pass2">Verify Your Email Address</p>
              <p className="otp">Enter 5 digit OTP sent on {this.props.Email} </p>
              <div >
              <input className="box" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
              <input className="box" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
              <input className="box" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
              <input className="box" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
              <input className="box" type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" /><br/><br/>
              </div>
              <button
                type="submit"
                className="btn1">
               Start Blogging
              </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default EmailVarify;