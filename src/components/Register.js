import React, { Component } from 'react'
import { register } from './UserFunctions'
import '../Style.css';


class Register extends Component {
  constructor() {
    super()
    this.state = {
      email: '',
      full_name: '',
      password: '',
      current_location: '',
      error: ''
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }
  dismissError = () => {
    this.setState({
        error:''
    })
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  
   
  onSubmit(e) {
    e.preventDefault()
  
    if(!this.state.email){
      return this.setState({
          error:'Email is required'
      });
  }
  const value = this.state.email
  var result = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
  if(result === false){
      this.setState({
        error:'Email is not valid'
      })
  }
    if(!this.state.password){
      return this.setState({
          error:'Password is required'
      });
  }
 
if(!this.state.full_name){
  return this.setState({
      error:'Full Name is required'
  });
}
if(!this.state.current_location){
  return this.setState({
      error:'Current Location is required'
  });
}
    const newUser = {
      email: this.state.email,
      full_name: this.state.full_name,
      password: this.state.password,
      current_location: this.state.current_location
    }
    
    register(newUser).then(res => {
      this.props.history.push(`/login`)
    })
    
  
      const p = this.state.password;
      if( p.test("?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20}")=== false)
         {
              return this.setState({
                  error:"password is not valid"
              })
          }  
     
      
      return this.setState({
          error:''
      });
    
  }
 
  render() {
    return (
      
      <div className="container">
        <div className="row">
          <div className="col-md-6 mt-5 mx-auto">
            <form noValidate onSubmit={this.onSubmit}>
              <h1 className="h3 mb-3 font-weight-normal">Register</h1>
              <div className="form-group">
              {  
                this.state.error && 
                <h3 style = {{color:'red'}} onClick = {this.dismissError}>
                <button onClick = {this.dismissError}>x</button>
                {this.state.error}
                </h3>
              }
                <input
                  type="text"
                  className="form-control fields1"
                  name="email"
                  placeholder=" Email"
                  value={this.state.email}
                  onChange={this.onChange}
                  required
                />
                
              </div>

              <div className="form-group">
                
                <input
                  type="password"
                  className="form-control fields1"
                  name="password"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={this.onChange}
                  required
                />
              </div>
              <div className="form-group">
               
                <input
                  type="text"
                  className="form-control fields1"
                  name="full_name"
                  placeholder="Full Name"
                  value={this.state.full_name}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
           
                <input
                  type="text"
                  className="form-control fields1"
                  name="current_location"
                  placeholder="Current Location"
                  value={this.state.current_location}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
              <input type="checkbox" className=" check" required /> I agree to all Terms & conditions
</div>
              <button
                type="submit"
                className="btn"
              >
                Sign Up
              </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default Register