import axios from 'axios'

export const register = newUser => {
  return axios
    .post('users/register', {
      Email: newUser.Email,
      Password: newUser.Password,
      Full_Name: newUser.Full_Name,
      Current_Location: newUser.Current_Location
    })
    .then(response => {
      console.log('Registered')
    })
}

export const login = user => {
  return axios
    .post('users/login', {
      Email: user.Email,
      Password: user.Password
    })
    .then(response => {
      localStorage.setItem('usertoken', response.data)
      return response.data
    })
    .catch(err => {
      console.log(err)
    })
}

export const getProfile = user => {
  return axios
    .get('users/Interest', {
      //headers: { Authorization: ` ${this.getToken()}` }
    })
    .then(response => {
      console.log(response)
      return response.data
    })
    .catch(err => {
      console.log(err)
    })
}