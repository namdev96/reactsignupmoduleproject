import React, { Component } from 'react'
import { login } from './UserFunctions'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../Style.css';
import { Link } from 'react-router-dom'


class Login extends Component {
  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
      error:''
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }
  dismissError = () => {
    this.setState({
        error:''
    })
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  onSubmit(e) {
    e.preventDefault()

    const user = {
      email: this.state.email,
      password: this.state.password
    }

    if(!this.state.email){
      return this.setState({
          error:'Email is required'
      });
  }
  if(!this.state.password){
      return this.setState({
          error:'Password is required'
      });
  }
  
  login(user).then(res => {
    if (res) {
      this.props.history.push(`/Interest`)
    }
  })
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-6 mt-5 mx-auto">
            <form noValidate onSubmit={this.onSubmit}>
              <div className="form-group">
              {  
                this.state.error && 
                <h3 style = {{color:'red'}} onClick = {this.dismissError}>
                <button onClick = {this.dismissError}>x</button>
                {this.state.error}
                </h3>
              }
             
                <input
                  type="email"
                  className="form-control fields1"
                  name="email"
                  placeholder="Email"
                  value={this.state.email}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
              
                <input
                  type="password"
                  className="form-control  fields1"
                  name="password"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
              </div>
              
            <a className ="pass">Forgot Password?</a><br/>
              <button
                type="submit"
                className="btn1 "
              >
                Sign in
              </button>
              <p className ="pass1"><pre>New to the site? <Link to ="register/"className ="">Sign up</Link></pre></p>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default Login